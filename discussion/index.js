// one line comment (ctrl + slash)
/*
	multiline comment (ctrl + shift + slash)
*/

console.log("Hello Batch 170!");

/*
	Javascript- we can see the messages/log in the console.

	Browser Consoles are part of the browsers which will allow us to see/log messages, data or information from the programming language
	
		they are easily accessed in the dev tools

	Statements
		instructions, expressions that we add to the programming language to communicate with the computers.

		usually ending with a semicolon (;). However, JS has implemented a way to automatically add semicolons at the end of the the line.
	
	Syntax
		set of rules that describes how statements should be made/constructed

		lines/blocks of codes must follow a certain set of rules for it to work properly
*/

console.log("Marco")
/*
	create three console logs to display your favorite food
		console.log("<yourFavoriteFood>")
*/
/*console.log("Adobo")
console.log("Adobo")
console.log("Adobo")*/

let food1 = "Adobo"

console.log(food1);

/*
	Variables are way to store information or data within the JS.

	to create a variable, we first declare the name of the variable with either let/const keyword:

		let nameOfVariable

	Then, we can initialize(define) the variable with a value or data
		
		let nameOfVariable = <valueOfVariable>
*/

console.log("My favorite food is: " + food1);

let food2 = "Kare Kare";

console.log("My favorite food is: " + food1 + " and " + food2);

let food3;

/*
	we can create variables without values, but the variables' content would log undefined.
*/
console.log(food3);

food3 = "Chickenjoy";
/*
	we can update the content of a variable by reassigning the value using an assignment operator (=);

	assignment operator (=) lets us (re)assign values to a variable
*/
console.log(food3);

/*
	we cannot create another variable with the same name. it will result into an error.

let food1 = "Flat Tops"

console.log(food1)
*/

// const keyword
/*
	const keyword will also allow creation of variable. However, with a const keyword, we create constant variable, which means the data saved in these variables will not change, cannot change and should not be changed.
*/

const pi = 3.1416;

console.log(pi);
/*
pi = "Pizza";

console.log(pi);
*/

/*
	we also cannot create a const variable without initialization or assigning its value.
const gravity;

console.log(gravity);
*/

/*
	Mini activity
	LET KEYWORD
	reassign a new value for food1 with another favorite food of yours
	reassign a new value for food2 with another favorite food of yours
	log the values of both variables in the console

	CONST KEYWORD
	create a const variable called sunriseDirection with East as its value
	create a const variable called sunsetDirection with West as its value
	log the values of both variables in the console
*/

food1 = "yema cake";
food2 = "lechon";
console.log(food1);
console.log(food2);

const sunriseDirection = "East";
const sunsetDirection = "West";
console.log(sunriseDirection);
console.log(sunsetDirection);

/*
	Guideline in creating a JS variable

	1. We can create a let variable with the let keyword. let variables can be reassigned but not redeclared
	
	2. Creating a variable has two parts: Declration of the variable name; and Initialization of the initial value of the variable throught the use of assignment operator (=).

	3. A let variable can be declared without initialization. However, the value of the variable will be undefined until it is re-assigned with a value.
	
	4. Not Defined cs Undefined. Not defined error means that the variable is used but not DECLARED. Undefined results from a variable that is used but not INITIALIZED.

	5. We can use const keyword to create variables. Constant variables cannot be declared without initialization. Constant variables values cannot be reaasigned.

	6. When creating variable names, start with small caps, this is because we are avoiding coonflict with JS that is named with capital letters

	7. If the variable would need two words, the naming convention is the use of camelCase. Do not add any space/s for the variables with words as names.

*/
// Data Types

/*
string
	are data types which are alphanumerical text. They could be a name, phrase, or even a sentence. We can create stringdata types with a single quote (') or with a double quote("). The text inside the quotation marks will be displayed as it is.

	keyword variable = "string data";
*/
console.log("Sample String Data");

/*
	numeric data types
		are data types which are numeric. numeric data types are displayed when numbers are not placed  in the quotation marks. if there is mathematical operation needed to be done, numeric data type should be used instead of string data type.

		keyword variable = numeric data;
*/
console.log(0123456789);
console.log(1+1);

// Mini activity
/*
	using string data type
		display in the console your:
			name (first and last)
			birthday
			province where you live

	using numeric data type
		display in the console your:
			high score in the last game you played
			favorite number
			your favorite number in electric fan/aircon
*/

console.log("Marco Kalalo");
console.log("September 11");
console.log("Batangas");

console.log(-1111);
console.log(73);
console.log(1);